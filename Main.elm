module Main exposing (..)

import AnimationFrame exposing (diffs)
import Color exposing (rgb)
import Collage exposing (filled, rect, collage)
import Element exposing (Element, toHtml)
import Html exposing (Html)
import Char exposing (KeyCode)
import Time exposing (Time)
import Keyboard


type Msg
    = TimeUpdate Time
    | KeyDown KeyCode
    | KeyUp KeyCode


type Key
    = Unknown


type alias Model =
    { screenWidth : Int
    , screenHeight : Int
    }


model : Model
model =
    { screenWidth = 500
    , screenHeight = 500
    }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ diffs TimeUpdate
        , Keyboard.downs KeyDown
        , Keyboard.ups KeyUp
        ]


fromCode : KeyCode -> Key
fromCode keyCode =
    case keyCode of
        _ ->
            Unknown


keyDown : KeyCode -> Model -> Model
keyDown keyCode model =
    case fromCode keyCode of
        _ ->
            model


keyUp : KeyCode -> Model -> Model
keyUp keyCode model =
    case fromCode keyCode of
        _ ->
            model


render : Model -> Element
render model =
    collage model.screenWidth
        model.screenHeight
        [ rect (toFloat model.screenWidth) (toFloat model.screenHeight) |> filled (rgb 120 100 120)
        ]


view : Model -> Html msg
view model =
    render model |> toHtml


updateTime : Time -> Model -> Model
updateTime dt model =
    model


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TimeUpdate dt ->
            ( updateTime dt model, Cmd.none )

        KeyDown keyCode ->
            ( keyDown keyCode model, Cmd.none )

        KeyUp keyCode ->
            ( keyDown keyCode model, Cmd.none )


init : ( Model, Cmd Msg )
init =
    ( model, Cmd.none )


main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
